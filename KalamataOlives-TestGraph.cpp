// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include <unordered_set>

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, edAC);
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    ASSERT_EQ(e, ++b);
    ASSERT_EQ(num_edges(g), 2);
    p = edges(g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(num_edges(g), 2);
    ASSERT_EQ(++++b, e);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdC, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);


    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;

    ASSERT_EQ(e, b);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;


    ASSERT_EQ(e, b);

    //ASSERT_EQ(*e, *b);   doesnt compile
    //ASSERT_EQ(*b, edAB); doesnt compile

}

TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;

    ASSERT_NE(e, b);

    ASSERT_EQ(*b, edAB);
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;




    ++b;

    //ASSERT_EQ(*b, *e); doesnt compile
    ASSERT_NE(b, e);
    //ASSERT_NE(*e, edAC);   doesnt compile
    //ASSERT_EQ(*--b, edAB); doesnt compile
    ++b;
    ASSERT_EQ(b,e);
    //ASSERT_EQ(*--e, edAB); doesnt compile

}

TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_iterator, edge_iterator> p = edges(g);

    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;

    ASSERT_EQ(b,e);

    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    p = edges(g);
    edge_iterator                      bb = p.first;
    edge_iterator                      ee = p.second;




    ASSERT_EQ(ee, e);

    ASSERT_NE(b,bb);
    ASSERT_EQ(b,ee);
    //++b;  //doesnt not compile
    ++bb;
    ++bb;

    ASSERT_EQ(bb,ee);

    p = edges(g);
    bb = p.first;
    ASSERT_NE(bb,ee);
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    ASSERT_EQ(*bb, edAC);

    p = edges(g);
    b = p.first;
    ASSERT_EQ(*b, edAB);
    ASSERT_EQ(*(b++), edAB);
    ASSERT_EQ(*(b), edAC);
    ASSERT_EQ(*(++b), edBC);

    //*b;     //does not compile
    *bb;    //compiles
    //*ee;    //does not compile
    //++ee; does not compile

    ++b;
    ASSERT_EQ(b,ee);

}

TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);



    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = edge(vdA, vdC, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      bb = p.first;
    edge_iterator                      ee = p.second;


    edge_descriptor ed = (*bb = edAC);
    ASSERT_EQ(ed,edAC);

    ASSERT_NE(*bb, edAC);

    p = edges(g);
    edge_iterator b = p.first;

    ASSERT_NE(*b, edAC);
    ASSERT_EQ(*b, *bb);
    if(*b == edAB) {
        ASSERT_EQ(*b, edAB);
        ASSERT_EQ(*++b, edBC);
    }
    else {
        ASSERT_EQ(*b, edBC);
        ASSERT_EQ(*++b, edAB);
    }

    ASSERT_EQ(++b, ee);
    ASSERT_EQ(num_edges(g), 2);

    //b += 2; //does not compile


    //ASSERT_EQ(ee - bb, 2);  //does not compile

}

TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);



    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;


    edge_iterator copy = b;
    edge_descriptor ed = *(b++);
    ASSERT_EQ(ed, *copy);
    ASSERT_NE(copy++, b);
    ASSERT_EQ(copy, b);

}
TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                      b = p.first;
    vertex_iterator                      e = p.second;


    ASSERT_EQ(*b, vdA);
    ASSERT_EQ(vdA, 0 );
    ASSERT_EQ(*(b++), vdA);
    ASSERT_EQ(*b, vdB);
    ASSERT_EQ(*++b, 2);
    ASSERT_EQ(++b, e);
    ASSERT_EQ(*e, 3);
}

TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA1 = add_vertex(g1);

    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB2 = add_vertex(g2);

    pair<vertex_iterator, vertex_iterator> p = vertices(g1);
    vertex_iterator                      b1 = p.first;
    vertex_iterator                      e1 = p.second;

    p = vertices(g2);
    vertex_iterator                      b2 = p.first;
    vertex_iterator                      e2 = p.second;

    ASSERT_EQ(b1, b2);
    ASSERT_EQ(*b1, *b2);
    ASSERT_EQ(vdA1, vdA2);
    ASSERT_NE(&vdA1, &vdA2);
}

TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdC1 = add_vertex(g1);

    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB2 = add_vertex(g2);
    vertex_descriptor vdC2 = add_vertex(g2);


    edge_descriptor edAB1 = add_edge(vdA1, vdB1, g1).first;
    edge_descriptor edAC1 = add_edge(vdA1, vdC1, g1).first;
    edge_descriptor edBC1 = add_edge(vdB1, vdC1, g1).first;

    edge_descriptor edAB2 = add_edge(vdA2, vdB2, g2).first;

    pair<edge_iterator, edge_iterator> p = edges(g1);
    edge_iterator                      b1 = p.first;
    edge_iterator                      e1 = p.second;

    p = edges(g2);
    edge_iterator                      b2 = p.first;
    edge_iterator                      e2 = p.second;


    ASSERT_NE(b1, b2);
    ASSERT_NE(*b1, *b2);
    ASSERT_NE(edAB1, edAB2);
    ASSERT_NE(&edAB1, &edAB2);
}

TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdC1 = add_vertex(g1);

    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB2 = add_vertex(g2);
    vertex_descriptor vdC2 = add_vertex(g2);

    vertex_descriptor v1 = vertex(0, g1);
    vertex_descriptor v2 = vertex(0, g2);


    ASSERT_EQ(v1, 0);
    ASSERT_EQ(vdB1, vdB2);
    ASSERT_EQ(vdC1, vdC2);
    ASSERT_EQ(v1, vdA1);
    ASSERT_EQ(v1,v2);
}

TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdC1 = add_vertex(g1);


    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB2 = add_vertex(g2);

    pair<edge_descriptor, bool> p = edge(vdA1, vdB1, g1);
    ASSERT_EQ(p.second, false);
    edge_descriptor e1 = p.first;
    edge_descriptor  e2 = edge(vdC1, vdA2, g1).first;

    edge_descriptor  e3 = edge(vdB1, vdC1, g2).first;

    ASSERT_EQ(e1, e2);
    ASSERT_EQ(e3, e2);
    ASSERT_EQ(p.second, false);
}

TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdC1 = add_vertex(g1);

    pair<edge_descriptor, bool> p = add_edge(vdB1, vdA1, g1);

    edge_descriptor e1 = p.first;

    ASSERT_EQ(p.second, true);

    p = edge(vdA1, vdB1, g1);

    edge_descriptor e2 = p.first;

    ASSERT_NE(e2, e1);

    ASSERT_EQ(p.second, false);

}

TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;


    pair<edge_descriptor, bool> p = add_edge(0, 1, g1);

    edge_descriptor e1 = p.first;

    ASSERT_EQ(p.second, true);


    ASSERT_EQ(num_vertices(g1), 2);

    ASSERT_EQ(num_edges(g1), 1);

}
TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;


    pair<edge_descriptor, bool> p = add_edge(0, 3, g1);

    edge_descriptor e1 = p.first;

    ASSERT_EQ(p.second, true);


    ASSERT_EQ(num_vertices(g1), 4);

    ASSERT_EQ(num_edges(g1), 1);

}

TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdB1 = add_vertex(g1);

    add_edge(0,1,g1);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0,g1);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(*b, vdB1);
    ASSERT_EQ(++b, e);
    ASSERT_EQ(num_vertices(g1), 2);
    ASSERT_EQ(num_edges(g1), 1);

    p = adjacent_vertices(1,g1);
    b = p.first;
    e = p.second;

    ASSERT_EQ(b,e);

    p = adjacent_vertices(6,g1);
    b = p.first;
    //*b; //does not compile
    ASSERT_EQ(num_vertices(g1), 2);
    ASSERT_EQ(num_edges(g1), 1);
}

TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    vertex_descriptor vdA1 = add_vertex(g1);

    vertex_descriptor vdB2 = add_vertex(g1);
    vertex_descriptor vdC2 = add_vertex(g1);

    vertex_iterator e1 = vertices(g1).second;

    //*b1 = 5;//does not compile
    ++++e1;
    ASSERT_EQ(*e1,5);
}

TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    vertex_descriptor vdA1 = add_vertex(g1);

    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdC1 = add_vertex(g1);

    edge_descriptor e1 = add_edge(vdA1, vdB1, g1).first;

    ASSERT_EQ(source(e1, g1), vdA1);
    ASSERT_NE(source(e1, g1), vdB1);
    ASSERT_EQ(target(e1, g1), vdB1);
    ASSERT_NE(target(e1, g1), vdA1);

}

TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    vertex_descriptor vdA1 = add_vertex(g1);

    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdC1 = add_vertex(g1);

    edge_descriptor e1 = add_edge(vdA1, vdB1, g1).first;
    edge_descriptor e2 = edge(vdA1, vdC1, g1).first;

    vertex_descriptor v1 = source(e2, g1);

    ASSERT_EQ(num_vertices(g1), 3);
    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(source(e1, g1), v1);

}
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    vertex_descriptor A = add_vertex(g1);
    vertex_descriptor B = add_vertex(g1);
    vertex_descriptor C = add_vertex(g1);
    add_edge(A,B,g1);
    add_edge(B,A,g1);
    add_edge(A,C,g1);

    adjacency_iterator b = adjacent_vertices(0, g1).first;
    adjacency_iterator e = adjacent_vertices(0, g1).second;

    if(*b == 1) {
        ASSERT_EQ(*b,1);
        //*b = 5; does not compile
        ASSERT_EQ(*++b,2);
    }
    else {
        ASSERT_EQ(*b,2);
        ASSERT_EQ(*++b,1);

    }

    ASSERT_EQ(++b, e);

    ASSERT_EQ(num_edges(g1), 3);

}
TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    vertex_descriptor A = add_vertex(g1);
    vertex_descriptor B = add_vertex(g1);
    edge_descriptor eAB1 = add_edge(A,B,g1).first;
    edge_descriptor eAB2 = edge(A,B,g1).first;
    edge_descriptor eAB3 = add_edge(A,B,g1).first;

    ASSERT_EQ(eAB1, eAB2);
    ASSERT_EQ(eAB3, eAB2);
    ASSERT_NE(&eAB2, &eAB1);

}
TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    vertex_descriptor A1 = add_vertex(g1);
    vertex_descriptor A2 = vertex(0, g2);

    ASSERT_EQ(A2, 0);
    ASSERT_EQ(A1, A2);

}

TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    vertex_descriptor A = add_vertex(g1);

    ASSERT_EQ(source(add_edge(A,A,g1).first, g1), A);
    ASSERT_EQ(target(add_edge(A,A,g1).first, g1), A);
    ASSERT_EQ(source(edge(A,A,g1).first, g1), A);
    //ASSERT_EQ(source(edge(A,A,g2).first, g1), A); does not compile
    edge_descriptor eAA = edge(A,A,g1).first;
    ASSERT_EQ(source(eAA, g2), A);

    ASSERT_EQ(source(eAA, g1), target(eAA,g1));
    ASSERT_EQ(source(eAA, g2), target(eAA,g1));
    ASSERT_EQ(source(eAA, g2), target(eAA, g2));

    ASSERT_EQ(num_vertices(g1), 1);
    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_vertices(g2), 0);
    ASSERT_EQ(num_edges(g2), 0);
}
TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor A1 = add_vertex(g1);
    vertex_descriptor A2 = add_vertex(g2);
    vertex_descriptor B1 = add_vertex(g1);


    ASSERT_EQ(vertex(0, g1), A1);
    ASSERT_EQ(vertex(0, g1), vertex(0,g2));
    ASSERT_EQ(vertex(1, g1), B1);
    ASSERT_EQ(vertex(1, g1), vertex(1,g2));
    ASSERT_EQ(vertex(150, g1), 150);
    ASSERT_EQ(vertex(70, g2), 70);
    ASSERT_EQ(num_vertices(g1), 2);
    ASSERT_EQ(num_edges(g1), 0);
    ASSERT_EQ(num_vertices(g2), 1);
    ASSERT_EQ(num_edges(g2), 0);
}
TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    vertex_descriptor A1 = add_vertex(g1);
    std::pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(A1, g1);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(b,e);
}
TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor A1 = add_vertex(g1);
    add_vertex(g2);
    vertex_descriptor B1 = add_vertex(g1);
    add_vertex(g2);

    add_edge(0,1,g1);
    add_edge(0,1,g2);

    std::pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(A1, g1);
    adjacency_iterator b1 = p.first;
    p = adjacent_vertices(A1, g2);
    adjacency_iterator b2 = p.first;

    ASSERT_EQ(*b1, *b2);
}
TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;


    add_edge(0,1,g1);
    add_edge(0,0,g1);
    add_edge(0,5,g1);
    add_edge(0,4,g1);
    add_edge(0,2,g1);
    add_edge(0,3,g1);

    std::pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0, g1);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_NE(b,e);

    int adj_counter = 0;

    while(b != e) {
        ++adj_counter;
        ++b;
    }


    ASSERT_EQ(b,e);
    ASSERT_EQ(adj_counter,6);
    ASSERT_EQ(num_edges(g1), 6);
    ASSERT_EQ(num_vertices(g1), 6);
}
TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;


    add_edge(0,1,g1);
    add_edge(0,5,g1);
    add_edge(0,1,g2);
    add_edge(0,5,g2);

    std::pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0, g1);
    adjacency_iterator b1 = p.first;
    adjacency_iterator e1 = p.second;

    p = adjacent_vertices(0,g2);
    adjacency_iterator b2 = p.first;
    adjacency_iterator e2 = p.second;

    ASSERT_NE(b1,e1);
    ASSERT_NE(b2,e2);
    ASSERT_NE(b1,b2);

    int adj_counter = 0;

    while(b1 != e1 || b2 != e2) {
        ++adj_counter;
        ASSERT_EQ(*b1, *b2);
        ++b1;
        ++b2;
    }


    ASSERT_EQ(b1,e1);
    ASSERT_EQ(b2,e2);
    ASSERT_EQ(adj_counter,2);
    ASSERT_EQ(num_edges(g1), 2);
    ASSERT_EQ(num_vertices(g1), 6);
    ASSERT_EQ(num_edges(g2), 2);
    ASSERT_EQ(num_vertices(g2), 6);
}
TYPED_TEST(GraphFixture, test31) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    add_edge(0,1,g1);
    add_edge(0,5,g1);
    add_edge(0,5,g1);

    std::pair<edge_iterator, edge_iterator> p = edges(g1);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    int edge_counter = 0;

    while(b != e) {
        ++edge_counter;
        ++b;
    }

    ASSERT_EQ(edge_counter,2);
    ASSERT_EQ(edge_counter,num_edges(g1));
}
TYPED_TEST(GraphFixture, test32) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);

    std::pair<vertex_iterator, vertex_iterator> p = vertices(g1);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;

    int vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter,4);
    ASSERT_EQ(vert_counter,num_vertices(g1));
}
TYPED_TEST(GraphFixture, test33) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    add_vertex(g1);
    add_vertex(g1);
    add_edge(0,3,g1);

    std::pair<vertex_iterator, vertex_iterator> p = vertices(g1);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;

    int vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter,4);
    ASSERT_EQ(vert_counter,num_vertices(g1));
}
TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);

    add_edge(0,1,g1);
    add_edge(1,2,g1);
    add_edge(2,0,g1);

    std::pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0,g1);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    int vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter, 1);


    p = adjacent_vertices(0,g1);
    b = p.first;
    e = p.second;

    vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter, 1);

    p = adjacent_vertices(0,g1);
    b = p.first;
    e = p.second;

    vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter, 1);

    ASSERT_EQ(num_vertices(g1), 3);
    ASSERT_EQ(num_edges(g1), 3);
}
TYPED_TEST(GraphFixture, test35) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g1;

    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);

    add_edge(0,1,g1);
    add_edge(1,0,g1);
    add_edge(1,2,g1);
    add_edge(2,1,g1);
    add_edge(2,0,g1);
    add_edge(0,2,g1);

    std::pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0,g1);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    int vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter, 2);


    p = adjacent_vertices(0,g1);
    b = p.first;
    e = p.second;

    vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter, 2);

    p = adjacent_vertices(0,g1);
    b = p.first;
    e = p.second;

    vert_counter = 0;
    while(b != e) {
        ++vert_counter;
        ++b;
    }

    ASSERT_EQ(vert_counter, 2);

    ASSERT_EQ(num_vertices(g1), 3);
    ASSERT_EQ(num_edges(g1), 6);
}
